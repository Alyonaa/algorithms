package com.company.graph2;

import java.util.ArrayList;
import java.util.Scanner;

public class ApplicationManagerForKrascal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        StorageForKrascal strg = new StorageForKrascal();
        int n = scanner.nextInt();
        int[] treeId = new int[n];
        ArrayList<StorageForKrascal> g = new ArrayList<>();
        scanner.nextLine();
        for (int i = 0; i < n; i++) {
            String s = scanner.nextLine();
            treeId[i] = i;
            int nCol = 0;
            int k = 0;
            boolean check = false;
            for (int j = 0; j < s.length(); j++) {
                if (s.charAt(j) >= '0' && s.charAt(j) <= '9') {
                    if (k == 0) {
                        k += s.charAt(j) - 48;
                    } else {
                        k *= 10;
                        k += s.charAt(j) - 48;
                    }
                    check = true;
                } else if (check) {
                    check = false;
                    if (k != 0) {
                        StorageForKrascal storage = new StorageForKrascal(k, i, nCol);
                        g.add(storage);
                    }
                    nCol++;
                    k = 0;
                }
            }
            if (check) {
                if (k != 0) {
                    StorageForKrascal storage = new StorageForKrascal(k, i, nCol);
                    g.add(storage);
                }
            }
        }
        GraphTasks2Solution graphTasks2Solution = new GraphTasks2Solution();
        strg.sortList(g);
        System.out.println(graphTasks2Solution.krascalAlgo(g, treeId));
    }
}
