package com.company.graph2;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static java.lang.Math.min;
import static java.util.Arrays.fill;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        ArrayDeque<Integer> queue = new ArrayDeque<>();
        HashMap<Integer, Integer> answer = new HashMap<>();
        boolean[] usedV = new boolean[adjacencyMatrix[0].length];
        int[] way = new int[adjacencyMatrix[0].length];
        queue.offer(startIndex);
        usedV[startIndex] = true;
        while (!queue.isEmpty()) {
            int v = queue.pollFirst();
            for (int j = 0; j < adjacencyMatrix[0].length; j++) {
                if (adjacencyMatrix[v][j] > 0 && !usedV[j]) {
                    queue.offer(j);
                    usedV[j] = true;
                    way[j] = way[v] + adjacencyMatrix[v][j];
                }
                if (adjacencyMatrix[v][j] > 0 && usedV[j] && way[v] + adjacencyMatrix[v][j] < way[j]) {
                    queue.offer(j);
                    way[j] = way[v] + adjacencyMatrix[v][j];
                }
            }

        }
        for (int i = 1; i < adjacencyMatrix[0].length; i++) {
            if (i != startIndex) {
                answer.put(i, way[i]);
            }
        }
        return answer;    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        int INF = Integer.MAX_VALUE / 2;
        int[] dist = new int[adjacencyMatrix[0].length];
        boolean[] used = new boolean[adjacencyMatrix[0].length];
        fill(dist, INF);
        dist[0] = 0;
        for (int i = 0; i < adjacencyMatrix[0].length; ++i) {
            int v = -1;
            for (int j = 0; j < adjacencyMatrix[0].length; ++j) {
                if (!used[j] && dist[j] < INF && (v == -1 || dist[v] > dist[j])) {
                    v = j;
                }
            }
            if (v == -1) {
                System.out.println("no way");
                System.exit(0);
            }
            used[v] = true;
            for (int to = 0; to < adjacencyMatrix[0].length; to++) {
                if (!used[to] && adjacencyMatrix[v][to] < INF && adjacencyMatrix[v][to] != 0) {
                    dist[to] = min(dist[to], adjacencyMatrix[v][to]);
                }
            }
        }
        return toGetSum(dist);
    }

    @Override
    public Integer krascalAlgo(ArrayList<StorageForKrascal> g, int[] treeId) {
        int sum = 0;
        for (StorageForKrascal storage : g) {
            if (treeId[storage.v1] != treeId[storage.v2]) {
                int old_id = treeId[storage.v2];
                int new_id = treeId[storage.v1];
                sum += storage.weight;
                for (int j = 0; j < treeId.length; ++j) {
                    if (treeId[j] == old_id) {
                        treeId[j] = new_id;
                    }
                }
            }
        }
        return sum;
    }

    public int toGetSum(int... ways) {
        return Arrays.stream(ways).sum();
    }
}
