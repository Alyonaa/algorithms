package com.company.graph2;

import java.util.Scanner;

public class ApplicationManagerForPrima {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        GraphTasks2Solution graphTasks2Solution = new GraphTasks2Solution();
        int n = scanner.nextByte();
        scanner.nextLine();
        int[][] adjacencyMatrix = new int[n][n];
        for (int i = 0; i < n; i++) {
            String s = scanner.nextLine();
            int nCol = 0;
            int k = 0;
            boolean check = false;
            for (int j = 0; j < s.length(); j++) {
                if (s.charAt(j) >= '0' && s.charAt(j) <= '9') {
                    if (k == 0) {
                        k += s.charAt(j) - 48;
                    } else {
                        k *= 10;
                        k += s.charAt(j) - 48;
                    }
                    check = true;
                } else if (check) {
                    check = false;
                    adjacencyMatrix[i][nCol] = k;
                    nCol++;
                    k = 0;
                }
            }
            if (check) {
                adjacencyMatrix[i][nCol] = k;
            }
        }
        System.out.println(graphTasks2Solution.primaAlgorithm(adjacencyMatrix));
    }
}

