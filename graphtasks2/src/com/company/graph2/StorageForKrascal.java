package com.company.graph2;

import java.util.ArrayList;
import java.util.Comparator;


public class StorageForKrascal {
    int weight;
    int v1;
    int v2;

    public StorageForKrascal() {
    }

    public StorageForKrascal(int weight, int v1, int v2) {
        this.weight = weight;
        this.v1 = v1;
        this.v2 = v2;
    }

    public int getWeight() {
        return weight;
    }

    public void sortList(ArrayList<StorageForKrascal> arr) {
        arr.sort(new Comparator<StorageForKrascal>() {
            @Override
            public int compare(StorageForKrascal o1, StorageForKrascal o2) {
                return o1.weight - o2.getWeight();

            }
        });
    }

}