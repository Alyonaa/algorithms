package com.company.graph2;

import java.util.Scanner;

public class ApplicationManagerForDijkstra {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] matrix = new int[n][n];
        scanner.nextLine();
        for (int i = 0; i < n; i++) {
            String s = scanner.nextLine();
            int nCol = 0;
            int k = 0;
            boolean check = false;
            for (int j = 0; j < s.length(); j++) {
                if (s.charAt(j) >= '0' && s.charAt(j) <= '9') {
                    if (k == 0) {
                        k += s.charAt(j) - 48;
                    } else {
                        k *= 10;
                        k += s.charAt(j) - 48;
                    }
                    check = true;
                } else if (check) {
                    check = false;
                    matrix[i][nCol] = k;
                    nCol++;
                    k = 0;
                }
            }
            if (check) {
                matrix[i][nCol] = k;
            }
        }
        int startV = scanner.nextInt();
        GraphTasks2Solution graphTasks2Solution = new GraphTasks2Solution();
        System.out.println(graphTasks2Solution.dijkstraSearch(matrix, startV));

    }
}
